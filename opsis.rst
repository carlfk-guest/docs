.. _opsis:

Opsis board
===========

This section does not cover how to flash an Opsis board with the HDMI2USB
firmware. If you are using a DebConf board, chances are that we already
flashed it for you. If you have to flash a board yourself, please consult the
`relevant documentation`_.

To access the Opsis board, you will need a TTY terminal application. We
recommend you use `tio`_ since it works and is available in Debian::

  $ apt install tio

Depending on your exact setup and the projector you use, it may be necessary to
connect to the board via USB serial to set up the right video modes. This can be
done using::

	$ tio /dev/ttyACM0

Here is a sample of the help menu::

	Available commands:
	help        - this command
	reboot      - reboot CPU
	mdio_dump   - dump mdio registers
	mdio_status - show mdio status
	pattern (p) - select next pattern

	status commands (alias: 's')
		status                         - print status message once
		status short                   - print status (short) message once
		status <ono              - repeatedly print status message
		status short <on/off>          - repeatedly print (short) status message

	video_matrix commands (alias: 'x')
		video_matrix list              - list available video sinks and sources
		x l                            - list available video sinks and sources
		video_matrix connect <source>  - connect video source to video sink
		 <sink>
		x c <source> <sink>            - connect video source to video sink

	video_mode commands (alias: 'm')
		video_mode list                - list available video modes
		m l                            - list available video modes
		video_mode <mode>              - select video mode
		video_mode custom <modeline>   - set custom video mode
		video_mode secondary <mode>    - select secondary video mode
		video_mode s <mode>            - select secondary video mode
		video_mode secondary off       - turn off secondary video mode

	change heartbeat status (alias: 'h')
		heartbeat <on/off>             - Turn on/off heartbeat feature

	hdp_toggle <source>              - toggle HDP on source for EDID rescan

	output0 commands (alias: 'o0')
		output0 on                     - enable output0
		output0 off                    - disable output0

	output1 commands (alias: 'o1')
		output1 on                     - enable output1
		output1 off                    - disable output1

	input0 commands (alias: 'i0')
		input0 on                     - enable input0
		input0 off                    - disable input0

	input1 commands (alias: 'i1')
		input1 on                     - enable input1
		input1 off                    - disable input1

	encoder commands (alias: 'e')
		encoder on                     - enable encoder
		encoder off                    - disable encoder
		encoder quality <quality>      - select quality
		encoder fps <fps>              - configure target fps

	debug commands (alias 'd')
		debug pll                      - dump pll configuration
		debug input0 <on/off>          - debug dvisampler0
		debug input1 <on/off>          - debug dvisampler1
		debug ddr                      - show DDR bandwidth
		debug dna                      - show Board's DNA
		debug edid                     - dump monitor EDID

.. _`relevant documentation`: https://hdmi2usb.tv/firmware/#flashing-prebuilt-firmware
.. _`tio`: https://tio.github.io/
