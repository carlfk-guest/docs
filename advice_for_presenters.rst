Advice for presenters
=====================

This is the reference documentation for advice for presenters for events
recorded by the DebConf video team. If you think something should be included
here, don't hesitate to :ref:`contact us` or to make a merge request on the `git
repository for this documentation`__.

__ https://salsa.debian.org/debconf-video-team/docs

All event-related advice should instead be added on their respective `wiki
page`__.

__ https://wiki.debian.org/Teams/DebConf/Video

Display Output
--------------

We use a :code:`16:9` resolution, :code:`1280x720`.
Our old limitation of :code:`1024x768` resolution and :code:`4:3` aspect ratio
no longer applies.
Slides that have a :code:`4:3` aspect ratio will be pillar-boxed.
This means if your presentation is not :code:`16:9`, it will be
displayed between black bars.

Our video capture system uses HDMI. The video team has adaptors to some other
formats:

* micro-HDMI
* mini-HDMI
* DVI
* DP
* mini-DP
* USB-C (thunderbolt compatible)
* VGA (best-effort, via unreliable adapters)

The VGA-to-HDMI adapters are on a best-effort basis. Those are known to be
somewhat unreliable and you are encouraged to test your laptop with them prior
to your talk, in order to check whether they are an option.

If your laptop doesn't have one of the above mentioned digital ports, it's up to
you to bring an adapter and, ideally, test it before bringing it to DebConf.

Presenters are strongly encouraged to test if their setup works correctly with
our video equipment before their session, either in one of the video team
volunteer training sessions or in breaks between talks. Attempting to fix stuff
five minutes before your presentation is not fun!

As a last resort, desktop systems are present as part of the videoteam setup,
so if you have your talk in PDF format, it can be presented via the desktop
system.

USB-C
^^^^^

If your USB-C port uses Thunderbolt, you may need to `update your thunderbolt
firmware`__ to get video to work.

__ https://daniel-lange.com/archives/129-Updating-the-Dell-XPS-13-9360-Thunderbolt-firmware-to-get-VGA-and-HDMI-working.html

xrandr hints
------------

720p VGA mode
^^^^^^^^^^^^^

One of these should work to get 720p video over VGA::

  xrandr --newmode 720p60 74.25  1280 1390 1430 1650 720 725 730 750 +HSync +VSync
  xrandr --newmode 720p60-2 74.25 1280 1392 1448 1650 720 722 728 750

Substitute the mode that works better appropriately in::

  xrandr --addmode VGA-1 720p60
  xrandr --output VGA-1 --mode 720p60

This should make VGA only laptops work with the Opsis capture board using a
VGA-to-HDMI adaptor.

Mirroring at non-native LCD resolution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This should work::

  xrandr --output LVDS-1 --scale-from 1280x720 --output HDMI-1 --same-as LVDS-1

Publishing your slides
----------------------

Please publish your slides in the appropriate `DebConf share repository`_.

Alternatively you can email your presentation the `videoteam mailing list`_ and
it will be added to the repository (eventually).

.. _DebConf share repository: https://salsa.debian.org/debconf-team/public/share
.. _`videoteam mailing list`: mailto:debconf-video@lists.debian.org

Templates
---------

Debian presentation templates for various architectures, and archives of
previous talks can be found on the main `Debian wiki`_.

.. _`Debian wiki`: https://wiki.debian.org/Presentations

LaTeX/beamer
^^^^^^^^^^^^

To get your slides to the right aspect ratio, you can use this snippet in your
beamer template::

  \documentclass[aspectratio=169]{beamer}

Water
-----

If possible, bring something to drink during the talk. We normally try to have
water bottles at the speaker's desk, but can't guarantee anything.
